package com.example.radarqueryservice.dtos;

import lombok.Data;

@Data
public class ChangeRadarSpeedLimitRequestDTO {

    private String radarId;
    private int speed;
}
