package com.example.radarqueryservice.events;


import lombok.Getter;
import com.example.radarqueryservice.enums.RadarStatus;

public class RadarStatusChangedEvent  extends BaseEvent<String> {
    @Getter private RadarStatus payload;

    public RadarStatusChangedEvent(String id, RadarStatus payload) {
        super(id);
        this.payload = payload;
    }



}
