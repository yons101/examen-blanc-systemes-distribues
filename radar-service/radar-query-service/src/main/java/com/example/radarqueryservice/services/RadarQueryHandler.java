package com.example.radarqueryservice.services;

import com.example.radarqueryservice.dtos.EventDataResponseDTO;
import com.example.radarqueryservice.dtos.OverSpeedResponseDTO;
import com.example.radarqueryservice.dtos.RadarOverSpeedsDTO;
import com.example.radarqueryservice.dtos.RadarResponseDTO;
import com.example.radarqueryservice.entities.OverSpeedDetection;
import com.example.radarqueryservice.entities.Radar;
import com.example.radarqueryservice.mappers.RadarMappers;
import com.example.radarqueryservice.queries.*;
import com.example.radarqueryservice.repositories.OverSpeedDetectionRepository;
import com.example.radarqueryservice.repositories.RadarRepository;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RadarQueryHandler {
    private RadarRepository radarRepository;
    private OverSpeedDetectionRepository overSpeedDetectionRepository;
    private RadarMappers radarMappers;


    public RadarQueryHandler(RadarRepository radarRepository, OverSpeedDetectionRepository overSpeedDetectionRepository, RadarMappers radarMappers) {
        this.radarRepository = radarRepository;
        this.overSpeedDetectionRepository = overSpeedDetectionRepository;
        this.radarMappers = radarMappers;
    }

    @QueryHandler
    public List<RadarResponseDTO> handler(GetAllRadarsQuery query) {
        List<Radar> allRadars = radarRepository.findAll();
        return allRadars.stream().map(radar -> radarMappers.from(radar))
                .collect(Collectors.toList());
    }


}
