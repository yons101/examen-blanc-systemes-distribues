package com.example.radarqueryservice.dtos;


import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EventDataResponseDTO<T> {




    private String type;
    private T eventData;

}
