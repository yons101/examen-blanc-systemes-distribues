package com.example.radarqueryservice.events;

import com.example.radarqueryservice.dtos.RadarDTO;

public class RadarCreatedEvent extends BaseEvent<String> {
    private RadarDTO payload;

    public RadarCreatedEvent(String id, RadarDTO payload) {
        super(id);
        this.payload = payload;
    }

    public RadarDTO getPayload() {
        return payload;
    }
}
