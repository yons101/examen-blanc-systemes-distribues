package com.example.radarqueryservice.dtos;

import lombok.Data;
import com.example.radarqueryservice.enums.RadarStatus;

@Data
public class ChangeRadarStatusRequestDTO {

    private String radarId;
    private RadarStatus radarStatus;
}
