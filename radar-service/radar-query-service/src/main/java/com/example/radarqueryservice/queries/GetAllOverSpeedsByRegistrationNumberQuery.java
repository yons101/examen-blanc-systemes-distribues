package com.example.radarqueryservice.queries;

public class GetAllOverSpeedsByRegistrationNumberQuery {
    private String registrationNumber;

    public GetAllOverSpeedsByRegistrationNumberQuery() {
    }

    public GetAllOverSpeedsByRegistrationNumberQuery(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
