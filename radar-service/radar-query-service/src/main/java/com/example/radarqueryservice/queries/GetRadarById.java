package com.example.radarqueryservice.queries;

public class GetRadarById {
    private String radarId;

    public GetRadarById(String radarId) {
        this.radarId = radarId;
    }

    public String getRadarId() {
        return radarId;
    }

    public void setRadarId(String radarId) {
        this.radarId = radarId;
    }
}
