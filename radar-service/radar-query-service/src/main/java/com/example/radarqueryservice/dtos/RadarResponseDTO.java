package com.example.radarqueryservice.dtos;

import lombok.Data;
import com.example.radarqueryservice.enums.RadarStatus;

@Data
public class RadarResponseDTO {

    private String radarId;
    private String name;
    private Double longitude;
    private Double latitude;
    private Double altitude;
    private Integer maxSpeed;
    private String roadDesignation;
    private RadarStatus radarStatus=RadarStatus.OFF;
}
