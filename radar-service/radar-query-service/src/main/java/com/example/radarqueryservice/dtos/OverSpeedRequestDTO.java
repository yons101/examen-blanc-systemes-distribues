package com.example.radarqueryservice.dtos;


import lombok.Data;
import com.example.radarqueryservice.enums.VehicleType;

import java.time.Instant;

@Data
public class OverSpeedRequestDTO {

    private String radarId;
    private Instant timeStamp;
    private String overSpeedId;
    private String vehicleRegistrationNumber;
    private VehicleType vehicleType;
    private int vehicleSpeed;
    private int radarMaxSpeed;
    private double radarLongitude;
    private double radarLatitude;
    private double radarAltitude;

}
