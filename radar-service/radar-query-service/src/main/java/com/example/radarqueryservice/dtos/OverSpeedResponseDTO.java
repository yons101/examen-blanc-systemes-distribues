package com.example.radarqueryservice.dtos;

import lombok.Data;

import java.time.Instant;

@Data
public class OverSpeedResponseDTO {

    private String overSpeedId;
    private Instant timeStamp;
    private String radarId;
    private String vehicleRegistrationNumber;
    private Integer vehicleSpeed;
}
