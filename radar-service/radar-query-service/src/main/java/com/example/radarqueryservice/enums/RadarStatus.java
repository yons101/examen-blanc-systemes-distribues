package com.example.radarqueryservice.enums;

public enum RadarStatus {
    ACTIVE, OUT_OF_ORDER, OFF
}
