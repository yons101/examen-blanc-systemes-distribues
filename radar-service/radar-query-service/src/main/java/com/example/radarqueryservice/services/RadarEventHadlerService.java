package com.example.radarqueryservice.services;


import com.example.radarqueryservice.dtos.EventDataResponseDTO;
import com.example.radarqueryservice.dtos.OverSpeedResponseDTO;
import com.example.radarqueryservice.entities.OverSpeedDetection;
import com.example.radarqueryservice.entities.Radar;
import com.example.radarqueryservice.events.RadarCreatedEvent;
import com.example.radarqueryservice.events.RadarSpeedLimitChangedEvent;
import com.example.radarqueryservice.events.RadarStatusChangedEvent;
import com.example.radarqueryservice.events.VehicleOverSpeedDetectedEvent;
import com.example.radarqueryservice.mappers.RadarMappers;
import com.example.radarqueryservice.queries.SubscribeToEventsQuery;
import com.example.radarqueryservice.repositories.OverSpeedDetectionRepository;
import com.example.radarqueryservice.repositories.RadarRepository;
import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.EventMessage;
import org.axonframework.eventhandling.Timestamp;
import org.axonframework.queryhandling.QueryUpdateEmitter;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;

@Service
@Transactional

public class RadarEventHadlerService {
    private RadarRepository radarRepository;
    private OverSpeedDetectionRepository overSpeedDetectionRepository;
    private RadarMappers radarMappers;
    private QueryUpdateEmitter queryUpdateEmitter;


    public RadarEventHadlerService(RadarRepository radarRepository, OverSpeedDetectionRepository overSpeedDetectionRepository, RadarMappers radarMappers, QueryUpdateEmitter queryUpdateEmitter) {
        this.radarRepository = radarRepository;
        this.overSpeedDetectionRepository = overSpeedDetectionRepository;
        this.radarMappers = radarMappers;
        this.queryUpdateEmitter = queryUpdateEmitter;
    }

    @EventHandler
    public void on(RadarCreatedEvent event, EventMessage<RadarCreatedEvent> eventMessage) {
        Radar radar = radarMappers.from(event.getPayload());
        radar.setRadarId(event.getId());
        radarRepository.save(radar);
        event.getPayload().setRadarId(event.getId());
        EventDataResponseDTO eventDataResponseDTO = new EventDataResponseDTO(
                event.getClass().getSimpleName(),
                event
        );
        queryUpdateEmitter.emit(SubscribeToEventsQuery.class,
                (query) -> true, eventDataResponseDTO);

    }

   

}
