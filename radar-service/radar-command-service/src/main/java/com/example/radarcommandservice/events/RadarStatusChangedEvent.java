package com.example.radarcommandservice.events;

import com.example.radarcommandservice.enums.RadarStatus;

public class RadarStatusChangedEvent  extends BaseEvent<String> {
    private RadarStatus payload;

    public RadarStatusChangedEvent(String id, RadarStatus payload) {
        super(id);
        this.payload = payload;
    }

    public RadarStatus getPayload() {
        return payload;
    }
}
