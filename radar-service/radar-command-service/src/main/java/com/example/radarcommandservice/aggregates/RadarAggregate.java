package com.example.radarcommandservice.aggregates;

import com.example.radarcommandservice.commands.ChangeRadarSpeedLimitCommand;
import com.example.radarcommandservice.commands.ChangeRadarStatusCommand;
import com.example.radarcommandservice.commands.CreateNewRadarCommand;
import com.example.radarcommandservice.commands.NewVehicleOverSpeedDetectionCommand;
import com.example.radarcommandservice.enums.RadarStatus;
import com.example.radarcommandservice.events.RadarCreatedEvent;
import com.example.radarcommandservice.events.RadarSpeedLimitChangedEvent;
import com.example.radarcommandservice.events.RadarStatusChangedEvent;
import com.example.radarcommandservice.events.VehicleOverSpeedDetectedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.modelling.command.AggregateMember;
import org.axonframework.spring.stereotype.Aggregate;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Aggregate

public class RadarAggregate {
    @AggregateIdentifier
    private String radarId;
    private String name;
    private double longitude;
    private double latitude;
    private  double altitude;
    private int maxSpeed;
    private String roadDesignation;
    private RadarStatus radarStatus;

    @AggregateMember
    private List<OverSpeedMember> overSpeedMembers =new ArrayList<>();

    public RadarAggregate() {
    }
    @CommandHandler
    public RadarAggregate(CreateNewRadarCommand command) {
        AggregateLifecycle.apply(new RadarCreatedEvent(
                command.getId(),
                command.getPayload()
        ));
    }
    @EventSourcingHandler
    public void on(RadarCreatedEvent event){
        this.radarId=event.getId();
        this.name=event.getPayload().getName();
        this.altitude=event.getPayload().getAltitude();
        this.longitude=event.getPayload().getLongitude();
        this.latitude=event.getPayload().getLatitude();
        this.radarStatus=event.getPayload().getRadarStatus();
        this.maxSpeed=event.getPayload().getMaxSpeed();
        this.roadDesignation=event.getPayload().getRoadDesignation();
    }
    @CommandHandler
    public void handle(ChangeRadarStatusCommand command){
        AggregateLifecycle.apply(new RadarStatusChangedEvent(
           command.getId(),
           command.getPayload()
        ));
    }
    @EventSourcingHandler
    public void on(RadarStatusChangedEvent event){
        this.radarId=event.getId();
        this.radarStatus=event.getPayload();
    }

    @CommandHandler
    public void handle(ChangeRadarSpeedLimitCommand command){
        AggregateLifecycle.apply(new RadarSpeedLimitChangedEvent(
                command.getId(),
                command.getPayload()
        ));
    }
    @EventSourcingHandler
    public void on(RadarSpeedLimitChangedEvent event){
        this.radarId=event.getId();
        this.maxSpeed=event.getPayload();
    }

    @CommandHandler
    public void handle(NewVehicleOverSpeedDetectionCommand command){
        command.getPayload().setOverSpeedId(UUID.randomUUID().toString());
        command.getPayload().setRadarMaxSpeed(this.maxSpeed);
        command.getPayload().setRadarLongitude(this.longitude);
        command.getPayload().setRadarLongitude(this.latitude);
        command.getPayload().setRadarAltitude(this.altitude);
        AggregateLifecycle.apply(new VehicleOverSpeedDetectedEvent(
                command.getId(),
                command.getPayload(),
                UUID.randomUUID().toString()
        ));
    }
    @EventSourcingHandler
    public void on(VehicleOverSpeedDetectedEvent event){
        this.radarId=event.getId();
        OverSpeedMember overSpeedMember =new OverSpeedMember();
        overSpeedMember.setId(UUID.randomUUID().toString());
        overSpeedMember.setVehicleRegistrationNumber(event.getPayload().getVehicleRegistrationNumber());
        overSpeedMember.setVehicleSpeed(event.getPayload().getVehicleSpeed());
        this.overSpeedMembers.add(overSpeedMember);
    }
}
