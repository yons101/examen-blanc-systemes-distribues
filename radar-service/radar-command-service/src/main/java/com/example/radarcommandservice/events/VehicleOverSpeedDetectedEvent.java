package com.example.radarcommandservice.events;

import lombok.Getter;
import com.example.radarcommandservice.dtos.OverSpeedRequestDTO;

public class VehicleOverSpeedDetectedEvent extends BaseEvent<String> {

    @Getter private String id;
    @Getter private OverSpeedRequestDTO payload;
    @Getter private String contraventionId;

    public VehicleOverSpeedDetectedEvent(String id, OverSpeedRequestDTO payload, String contraventionId) {
        super(id);
        this.payload = payload;
        this.contraventionId = contraventionId;
    }

}
