package com.example.radarcommandservice.dtos;

import lombok.Data;

@Data
public class ChangeRadarSpeedLimitRequestDTO {

    private String radarId;
    private int speed;
}
