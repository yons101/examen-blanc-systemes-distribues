package com.example.radarcommandservice.events;

import com.example.radarcommandservice.dtos.RadarDTO;

public class RadarCreatedEvent extends BaseEvent<String> {
    private RadarDTO payload;

    public RadarCreatedEvent(String id, RadarDTO payload) {
        super(id);
        this.payload = payload;
    }

    public RadarDTO getPayload() {
        return payload;
    }
}
