package com.example.radarcommandservice.dtos;

import lombok.Data;
import com.example.radarcommandservice.enums.RadarStatus;

@Data
public class RadarDTO {
    private String radarId;
    private String name;
    private Double longitude;
    private Double latitude;
    private Double altitude;
    private Integer maxSpeed;
    private String roadDesignation;
    private RadarStatus radarStatus;

    //json format




}
