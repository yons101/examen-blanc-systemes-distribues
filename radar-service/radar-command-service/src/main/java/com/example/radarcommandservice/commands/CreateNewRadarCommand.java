package com.example.radarcommandservice.commands;

import lombok.Getter;
import com.example.radarcommandservice.dtos.RadarDTO;

public class CreateNewRadarCommand extends BaseCommand<String> {

    @Getter private RadarDTO payload;
    public CreateNewRadarCommand(String id, RadarDTO payload) {
        super(id);
        this.payload = payload;
    }

}
