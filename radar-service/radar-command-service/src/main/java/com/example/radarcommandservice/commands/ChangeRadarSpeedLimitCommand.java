package com.example.radarcommandservice.commands;

import lombok.Getter;

public class ChangeRadarSpeedLimitCommand  extends BaseCommand<String> {
    @Getter private int payload;
    public ChangeRadarSpeedLimitCommand(String id, int payload) {
        super(id);
        this.payload = payload;
    }

}
