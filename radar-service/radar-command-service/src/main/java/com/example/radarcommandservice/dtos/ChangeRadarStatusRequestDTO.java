package com.example.radarcommandservice.dtos;

import lombok.Data;
import com.example.radarcommandservice.enums.RadarStatus;

@Data
public class ChangeRadarStatusRequestDTO {

    private String radarId;
    private RadarStatus radarStatus;
}
