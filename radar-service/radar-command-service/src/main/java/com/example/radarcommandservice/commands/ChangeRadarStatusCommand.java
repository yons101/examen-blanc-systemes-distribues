package com.example.radarcommandservice.commands;

import lombok.Getter;
import com.example.radarcommandservice.enums.RadarStatus;

public class ChangeRadarStatusCommand  extends BaseCommand<String> {
    @Getter private RadarStatus payload;
    public ChangeRadarStatusCommand(String id, RadarStatus payload) {
        super(id);
        this.payload = payload;
    }

}
