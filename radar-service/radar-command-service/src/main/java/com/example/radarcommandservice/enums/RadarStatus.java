package com.example.radarcommandservice.enums;

public enum RadarStatus {
    ACTIVE, OUT_OF_ORDER, OFF
}
