package com.example.radarcommandservice.events;

import lombok.Getter;

public class RadarSpeedLimitChangedEvent extends BaseEvent<String> {
    @Getter private int payload;

    public RadarSpeedLimitChangedEvent(String id, int payload) {
        super(id);
        this.payload = payload;
    }


}
