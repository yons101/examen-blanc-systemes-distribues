package com.example.radarcommandservice.commands;

import com.example.radarcommandservice.dtos.OverSpeedRequestDTO;

public class NewVehicleOverSpeedDetectionCommand extends BaseCommand<String> {
    private OverSpeedRequestDTO payload;

    public NewVehicleOverSpeedDetectionCommand(String id, OverSpeedRequestDTO payload) {
        super(id);
        this.payload = payload;
    }

    public OverSpeedRequestDTO getPayload() {
        return payload;
    }
}
