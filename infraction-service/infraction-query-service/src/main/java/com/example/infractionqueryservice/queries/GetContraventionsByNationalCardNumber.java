package com.example.infractionqueryservice.queries;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GetContraventionsByNationalCardNumber {

    private String nationalCardNumber;
    private int page;
    private int size;

}
