package com.example.infractionqueryservice.repositories;

import com.example.infractionqueryservice.entities.Contravention;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContraventionRepository extends JpaRepository<Contravention,String> {
    Page<Contravention> findAllByOwnerNationalCardId(String ncID, Pageable pageable);
}
