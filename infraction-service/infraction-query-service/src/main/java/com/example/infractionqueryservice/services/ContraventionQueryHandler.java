package com.example.infractionqueryservice.services;

import com.example.infractionqueryservice.queries.GetContraventionsByNationalCardNumber;
import com.example.infractionqueryservice.repositories.ContraventionRepository;
import org.axonframework.queryhandling.QueryHandler;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
public class ContraventionQueryHandler {
    private ContraventionRepository contraventionRepository;

    public ContraventionQueryHandler(ContraventionRepository contraventionRepository) {
        this.contraventionRepository = contraventionRepository;
    }

    @QueryHandler
    public Page on(GetContraventionsByNationalCardNumber query){
        if(query.getNationalCardNumber().equals(null) ||
        query.getNationalCardNumber().equals("") ||
        query.getNationalCardNumber().equals("undefined")){
            return contraventionRepository.findAll(PageRequest.of(query.getPage(),query.getSize()));
        }
        else return contraventionRepository.findAllByOwnerNationalCardId(query.getNationalCardNumber(), PageRequest.of(query.getPage(), query.getSize()));
    }
}
