package com.example.infractionqueryservice.commands;

import com.example.infractionqueryservice.dtos.OverSpeedRequestDTO;
import lombok.Getter;

public class NewContraventionCommand extends BaseCommand<String> {

    @Getter private OverSpeedRequestDTO payload;

    public NewContraventionCommand(String id, OverSpeedRequestDTO payload) {
        super(id);
        this.payload = payload;
    }
}
