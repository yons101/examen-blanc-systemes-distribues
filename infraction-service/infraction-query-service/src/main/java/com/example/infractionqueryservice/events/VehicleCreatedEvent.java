package com.example.infractionqueryservice.events;


import com.example.infractionqueryservice.dtos.VehicleRequestDTO;

public class VehicleCreatedEvent extends BaseEvent<String> {
    private VehicleRequestDTO payload;

    public VehicleCreatedEvent(String id, VehicleRequestDTO payload) {
        super(id);
        this.payload = payload;
    }

    public VehicleRequestDTO getPayload() {
        return payload;
    }
}
