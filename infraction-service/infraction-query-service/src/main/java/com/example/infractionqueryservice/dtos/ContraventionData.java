package com.example.infractionqueryservice.dtos;


import lombok.Data;
import com.example.infractionqueryservice.enums.ContraventionStatus;

@Data
public class ContraventionData extends OverSpeedRequestDTO {
    private String contraventionId;
    private Double amount;
    private String vehicleOwner;
    private String ownerEmail;
    private String ownerPhoneNumber;
    private String ownerAddress;
    private String ownerNationalCardId;
    private ContraventionStatus status;
}
