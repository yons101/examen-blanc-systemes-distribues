package com.example.infractionqueryservice.events;

import lombok.Getter;
import com.example.infractionqueryservice.dtos.OverSpeedRequestDTO;

public class VehicleOverSpeedDetectedEvent extends BaseEvent<String> {

    @Getter
    private String contraventionId;
    @Getter
    private OverSpeedRequestDTO payload;

    public VehicleOverSpeedDetectedEvent(String id, OverSpeedRequestDTO payload, String contraventionId) {
        super(id);
        this.payload = payload;
        this.contraventionId = contraventionId;
    }

}
