package com.example.infractionqueryservice.enums;

public enum ContraventionStatus {
    PENDING, VALIDATED, PAID
}

