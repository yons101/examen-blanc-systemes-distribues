package com.example.infractionqueryservice.events;

import lombok.Getter;
import com.example.infractionqueryservice.dtos.ContraventionData;

public class ContraventionCreatedEvent extends BaseEvent<String> {

    @Getter private ContraventionData payload;

    public ContraventionCreatedEvent(String id, ContraventionData payload) {
        super(id);
        this.payload = payload;
    }
}
