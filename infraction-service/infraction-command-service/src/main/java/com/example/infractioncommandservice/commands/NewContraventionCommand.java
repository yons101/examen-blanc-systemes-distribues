package com.example.infractioncommandservice.commands;

import lombok.Getter;
import com.example.infractioncommandservice.dtos.OverSpeedRequestDTO;

public class NewContraventionCommand extends BaseCommand<String> {

    @Getter private OverSpeedRequestDTO payload;

    public NewContraventionCommand(String id, OverSpeedRequestDTO payload) {
        super(id);
        this.payload = payload;
    }
}
