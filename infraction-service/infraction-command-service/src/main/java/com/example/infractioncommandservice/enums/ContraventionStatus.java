package com.example.infractioncommandservice.enums;

public enum ContraventionStatus {
    PENDING, VALIDATED, PAID
}

