package com.example.infractioncommandservice.events;

import lombok.Getter;
import com.example.infractioncommandservice.dtos.OverSpeedRequestDTO;

public class VehicleOverSpeedDetectedEvent extends BaseEvent<String> {
   /*
   *
   *     override val id : String,
    val payload : OverSpeedRequestDTO,
    val contraventionId : String,
    *
    * */

    @Getter
    private String contraventionId;
    @Getter
    private OverSpeedRequestDTO payload;

    public VehicleOverSpeedDetectedEvent(String id, OverSpeedRequestDTO payload, String contraventionId) {
        super(id);
        this.payload = payload;
        this.contraventionId = contraventionId;
    }

}
