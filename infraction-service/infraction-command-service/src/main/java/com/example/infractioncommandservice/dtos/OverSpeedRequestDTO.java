package com.example.infractioncommandservice.dtos;


import lombok.Data;
import com.example.infractioncommandservice.enums.VehicleType;

@Data
public class OverSpeedRequestDTO {

    private String radarId;
    private String timeStamp;
    private String overSpeedId;
    private String vehicleRegistrationNumber;
    private VehicleType vehicleType;
    private int vehicleSpeed;
    private int radarMaxSpeed;
    private double radarLongitude;
    private double radarLatitude;
    private double radarAltitude;

}
