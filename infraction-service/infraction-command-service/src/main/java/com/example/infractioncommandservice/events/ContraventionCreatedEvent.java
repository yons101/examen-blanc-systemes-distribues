package com.example.infractioncommandservice.events;

import lombok.Getter;
import com.example.infractioncommandservice.dtos.ContraventionData;

public class ContraventionCreatedEvent extends BaseEvent<String> {

    @Getter private ContraventionData payload;

    public ContraventionCreatedEvent(String id, ContraventionData payload) {
        super(id);
        this.payload = payload;
    }
}
