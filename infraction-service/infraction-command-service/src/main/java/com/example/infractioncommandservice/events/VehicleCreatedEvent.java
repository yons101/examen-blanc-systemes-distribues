package com.example.infractioncommandservice.events;


import com.example.infractioncommandservice.dtos.VehicleRequestDTO;

public class VehicleCreatedEvent extends BaseEvent<String> {
    private VehicleRequestDTO payload;

    public VehicleCreatedEvent(String id, VehicleRequestDTO payload) {
        super(id);
        this.payload = payload;
    }

    public VehicleRequestDTO getPayload() {
        return payload;
    }
}
