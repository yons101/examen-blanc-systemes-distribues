package com.example.immatriculationcommandservice.aggregates;

import com.example.immatriculationcommandservice.commands.CreateVehicleCommand;
import com.example.immatriculationcommandservice.commands.UpdateVehicleOwnerCommand;
import com.example.immatriculationcommandservice.enums.VehicleType;
import com.example.immatriculationcommandservice.events.VehicleCreatedEvent;
import com.example.immatriculationcommandservice.events.VehicleOwnerUpdatedEvent;
import org.axonframework.commandhandling.CommandHandler;
import org.axonframework.eventsourcing.EventSourcingHandler;
import org.axonframework.modelling.command.AggregateIdentifier;
import org.axonframework.modelling.command.AggregateLifecycle;
import org.axonframework.spring.stereotype.Aggregate;

@Aggregate
public class VehicleRegistrationAggregate {
    @AggregateIdentifier
    private String registrationNumber;
    private VehicleType type;
    private String brand;
    private String model;
    private int fiscalPower;
    private String ownerName;
    private String ownerNationalIdCard;
    private String ownerEmail;
    private String ownerPhoneNumber;
    private String ownerAddress;

    public VehicleRegistrationAggregate() {
    }

    @CommandHandler
    public VehicleRegistrationAggregate(CreateVehicleCommand command) {
        AggregateLifecycle.apply(new VehicleCreatedEvent(
                command.getId(),
                command.getPayload()
        ));
    }

    @EventSourcingHandler
    public void on(VehicleCreatedEvent event) {
        this.registrationNumber = event.getId();
        this.brand = event.getPayload().getBrand();
        this.fiscalPower = event.getPayload().getFiscalPower();
        this.model = event.getPayload().getModel();
        this.type = event.getPayload().getType();
        this.ownerName = event.getPayload().getOwnerName();
        this.ownerEmail = event.getPayload().getOwnerEmail();
        this.ownerAddress = event.getPayload().getOwnerAddress();
        this.ownerNationalIdCard = event.getPayload().getOwnerNationalIdCard();
        this.ownerPhoneNumber = event.getPayload().getOwnerPhoneNumber();
    }

    @CommandHandler
    public void handle(UpdateVehicleOwnerCommand command) {
        AggregateLifecycle.apply(new VehicleOwnerUpdatedEvent(
                command.getId(),
                command.getPayload()
        ));
    }

    @EventSourcingHandler
    public void on(VehicleOwnerUpdatedEvent event) {
        this.registrationNumber = event.getId();
        this.ownerName = event.getPayload().getOwnerName();
        this.ownerEmail = event.getPayload().getOwnerEmail();
        this.ownerAddress = event.getPayload().getOwnerAddress();
        this.ownerNationalIdCard = event.getPayload().getOwnerNationalIdCard();
        this.ownerPhoneNumber = event.getPayload().getOwnerPhoneNumber();
    }
}
