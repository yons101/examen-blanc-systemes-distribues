package com.example.immatriculationcommandservice.commands;

import com.example.immatriculationcommandservice.dtos.UpdateVehicleOwnerRequestDTO;
import lombok.Getter;

public class UpdateVehicleOwnerCommand extends BaseCommand<String> {

    @Getter private UpdateVehicleOwnerRequestDTO payload;
    public UpdateVehicleOwnerCommand(String id, UpdateVehicleOwnerRequestDTO payload) {
        super(id);
        this.payload = payload;
    }


}
