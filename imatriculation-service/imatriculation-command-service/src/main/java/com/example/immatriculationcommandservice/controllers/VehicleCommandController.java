package com.example.immatriculationcommandservice.controllers;

import com.example.immatriculationcommandservice.commands.CreateVehicleCommand;
import com.example.immatriculationcommandservice.commands.UpdateVehicleOwnerCommand;
import com.example.immatriculationcommandservice.dtos.UpdateVehicleOwnerRequestDTO;
import com.example.immatriculationcommandservice.dtos.VehicleRequestDTO;

import org.axonframework.commandhandling.gateway.CommandGateway;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("/commands")
@CrossOrigin("*")
public class VehicleCommandController {
    private CommandGateway commandGateway;

    public VehicleCommandController(CommandGateway commandGateway) {
        this.commandGateway = commandGateway;
    }

    @PostMapping("/vehicles/create")
    public CompletableFuture<String> createNewVehicle(@RequestBody VehicleRequestDTO request){
        return this.commandGateway.send(new CreateVehicleCommand(
           request.getRegistrationNumber(),
           request
        ));
    }
    @PutMapping("/vehicles/updateOwner")
    public CompletableFuture<String> updateOwner(@RequestBody UpdateVehicleOwnerRequestDTO request){
        return this.commandGateway.send(new UpdateVehicleOwnerCommand(
                request.getRegistrationNumber(),
                request
        ));
    }
}
