package com.example.immatriculationcommandservice.events;

import com.example.immatriculationcommandservice.dtos.VehicleRequestDTO;
import lombok.Getter;

public class VehicleCreatedEvent extends BaseEvent<String> {
    @Getter private VehicleRequestDTO payload;
    public VehicleCreatedEvent(String id, VehicleRequestDTO payload) {
        super(id);
        this.payload = payload;
    }
}
