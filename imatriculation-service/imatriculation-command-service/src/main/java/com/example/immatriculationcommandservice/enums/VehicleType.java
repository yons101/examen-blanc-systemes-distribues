package com.example.immatriculationcommandservice.enums;

public enum  VehicleType {
    CAR, BUS, TRUCK, MOTOCYCLE
}
