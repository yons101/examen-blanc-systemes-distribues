package com.example.immatriculationcommandservice.dtos;

import com.example.immatriculationcommandservice.enums.VehicleType;
import lombok.Data;

@Data
public class VehicleRequestDTO {
    private String registrationNumber;
    private VehicleType type;
    private String brand;
    private String model;
    private int fiscalPower;
    private String ownerName;
    private String ownerNationalIdCard;
    private String ownerEmail;
    private String ownerPhoneNumber;
    private String ownerAddress;



}
