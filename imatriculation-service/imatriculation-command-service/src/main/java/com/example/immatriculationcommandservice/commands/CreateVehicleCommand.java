package com.example.immatriculationcommandservice.commands;

import com.example.immatriculationcommandservice.dtos.VehicleRequestDTO;
import lombok.Getter;

public class CreateVehicleCommand extends BaseCommand<String> {

    @Getter private VehicleRequestDTO payload;
    public CreateVehicleCommand(String id, VehicleRequestDTO payload) {
        super(id);
        this.payload = payload;
    }


}

