package com.example.imatriculatonqueryservice.events;

import com.example.imatriculatonqueryservice.dtos.UpdateVehicleOwnerRequestDTO;
import lombok.Getter;
public class VehicleOwnerUpdatedEvent {
    @Getter private String id;
    @Getter private UpdateVehicleOwnerRequestDTO payload;
    public VehicleOwnerUpdatedEvent(String id, UpdateVehicleOwnerRequestDTO payload) {
        this.id = id;
        this.payload = payload;
    }

}
