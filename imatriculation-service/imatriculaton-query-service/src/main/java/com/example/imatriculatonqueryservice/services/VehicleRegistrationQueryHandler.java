package com.example.imatriculatonqueryservice.services;

import com.example.imatriculatonqueryservice.entities.Vehicle;
import com.example.imatriculatonqueryservice.entities.VehicleOwner;
import com.example.imatriculatonqueryservice.queries.GetAllOwners;
import com.example.imatriculatonqueryservice.queries.GetAllVehiclesQuery;
import com.example.imatriculatonqueryservice.repositories.VehicleOwnerRepository;
import com.example.imatriculatonqueryservice.repositories.VehicleRepository;


import org.axonframework.queryhandling.QueryHandler;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service

@Transactional
public class VehicleRegistrationQueryHandler {
    private VehicleRepository vehicleRepository;
    private VehicleOwnerRepository vehicleOwnerRepository;

    public VehicleRegistrationQueryHandler(VehicleRepository vehicleRepository, VehicleOwnerRepository vehicleOwnerRepository) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleOwnerRepository = vehicleOwnerRepository;
    }

    @QueryHandler
    public List<Vehicle> vehicles(GetAllVehiclesQuery query) {
        return vehicleRepository.findAll();
    }

    @QueryHandler
    public List<VehicleOwner> owners(GetAllOwners query) {
        return vehicleOwnerRepository.findAll();
    }



}
