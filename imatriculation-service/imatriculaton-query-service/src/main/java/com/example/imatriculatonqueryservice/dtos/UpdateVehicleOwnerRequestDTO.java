package com.example.imatriculatonqueryservice.dtos;

import lombok.Data;

@Data
public class UpdateVehicleOwnerRequestDTO {
    private String registrationNumber;
    private String ownerName;
    private String ownerAddress;
    private String ownerPhoneNumber;
    private String ownerEmail;
    private String ownerNationalIdCard;

}
