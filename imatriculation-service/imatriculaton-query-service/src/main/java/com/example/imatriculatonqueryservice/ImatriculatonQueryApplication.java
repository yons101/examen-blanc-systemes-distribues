package com.example.imatriculatonqueryservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImatriculatonQueryApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImatriculatonQueryApplication.class, args);
    }

}
