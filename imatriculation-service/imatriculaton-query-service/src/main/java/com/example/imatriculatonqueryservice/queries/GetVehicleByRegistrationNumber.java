package com.example.imatriculatonqueryservice.queries;

public class GetVehicleByRegistrationNumber {

    private String registrationNumber;

    public GetVehicleByRegistrationNumber() {
    }

    public GetVehicleByRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
