package com.example.imatriculatonqueryservice.services;

import com.example.imatriculatonqueryservice.entities.Vehicle;
import com.example.imatriculatonqueryservice.entities.VehicleOwner;
import com.example.imatriculatonqueryservice.events.VehicleCreatedEvent;
import com.example.imatriculatonqueryservice.events.VehicleOwnerUpdatedEvent;
import com.example.imatriculatonqueryservice.repositories.VehicleOwnerRepository;
import com.example.imatriculatonqueryservice.repositories.VehicleRepository;

import org.axonframework.eventhandling.EventHandler;
import org.axonframework.eventhandling.Timestamp;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;

@Service
@Transactional
public class VehicleRegistrationEventHandler {
    private VehicleRepository vehicleRepository;
    private VehicleOwnerRepository vehicleOwnerRepository;

    public VehicleRegistrationEventHandler(VehicleRepository vehicleRepository, VehicleOwnerRepository vehicleOwnerRepository) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleOwnerRepository = vehicleOwnerRepository;
    }

    @EventHandler
    public void on(VehicleCreatedEvent event) {
        Vehicle vehicle = new Vehicle();
        vehicle.setRegistrationNumber(event.getId());
        vehicle.setModel(event.getPayload().getModel());
        vehicle.setType(event.getPayload().getType());
        vehicle.setBrand(event.getPayload().getBrand());
        vehicle.setFiscalPower(event.getPayload().getFiscalPower());

        VehicleOwner vehicleOwner = vehicleOwnerRepository.findByOwnerNationalIdCard(event.getPayload().getOwnerNationalIdCard());
        if (vehicleOwner == null) {
            vehicleOwner = new VehicleOwner();
            vehicleOwner.setOwnerName(event.getPayload().getOwnerName());
            vehicleOwner.setOwnerEmail(event.getPayload().getOwnerEmail());
            vehicleOwner.setOwnerAddress(event.getPayload().getOwnerAddress());
            vehicleOwner.setOwnerPhoneNumber(event.getPayload().getOwnerPhoneNumber());
            vehicleOwner.setOwnerNationalIdCard(event.getPayload().getOwnerNationalIdCard());
            vehicleOwner.setId(UUID.randomUUID().toString());
            vehicleOwner = vehicleOwnerRepository.save(vehicleOwner);
        }
        Vehicle saveVehicle = vehicleRepository.save(vehicle);
    }

}
