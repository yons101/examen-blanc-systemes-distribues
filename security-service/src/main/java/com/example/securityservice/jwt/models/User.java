package com.example.securityservice.jwt.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "USERS",
uniqueConstraints = {
		@UniqueConstraint(columnNames = "username"),
		@UniqueConstraint(columnNames = "email")
})
public class User {
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY)
	private Long id;
	@NotBlank
	@Size(max = 50)
	private String email;
	@NotBlank
	@Size (max = 20)
	private String username;
	@NotBlank
	@Size(max = 120)
	private String password;
	@ManyToMany(fetch = FetchType.LAZY)
	  @JoinTable(  name = "user_roles", 
	        joinColumns =  @JoinColumn (name = "user_id"), 
	        inverseJoinColumns =  	@JoinColumn (name = "role_id"))
	private Set<Role> roles = new HashSet<>();
	public User(@NotBlank @Email @Size(max = 50) String email, @NotBlank @Size(max = 20) String username,
			@NotBlank @Size(max = 120) String password) {
		super();
		this.email = email;
		this.username = username;
		this.password = password;
	}
	
	
}
